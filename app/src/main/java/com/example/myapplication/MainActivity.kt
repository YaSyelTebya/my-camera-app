package com.example.myapplication

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat

class MainActivity : AppCompatActivity() {

    private val cameraRequestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                onCameraIsAllowed()
            } else {
                onCameraIsNotAllowed()
            }
        }

    private lateinit var imageView: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        imageView = findViewById(R.id.result_image)

        if (ContextCompat.checkSelfPermission(this, CAMERA_PERMISSION)
            == PackageManager.PERMISSION_GRANTED) {
            Log.i(LOG_TAG, "Already allowed")
            onCameraIsAllowed()
        } else {
            Log.i(LOG_TAG, "Requesting camera permission")
            cameraRequestPermissionLauncher.launch(
                CAMERA_PERMISSION)
        }
    }

    private fun onCameraIsNotAllowed() = afterRequestingPermission {
        imageView.setImageResource(IMAGE_ON_PERMISSION_REQUEST_DENY)
        showRequestCameraResult(getString(ACCESS_DENIED_MESSAGE_ID),
            "Camera is not allowed")
    }

    private fun onCameraIsAllowed() = afterRequestingPermission {
        showRequestCameraResult(getString(ACCESS_GRANTED_MESSAGE_ID),
            "Camera is allowed")
        takePhoto()
    }

    private fun showRequestCameraResult(toastMessage: String, logMessage: String) {
        Log.i(LOG_TAG, logMessage)
        toastMessage.showInToast()
    }

    private fun afterRequestingPermission(action: () -> Unit) {
        action()
        showPicture()
    }

    private fun showPicture() {
        imageView.visibility = View.VISIBLE
    }

    private fun takePhoto() {
        val cameraIntent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(cameraIntent, TAKE_PHOTO_FROM_CAMERA_REQUEST_CODE)
    }

    @Deprecated("Deprecated in Java")
    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        data ?: return
        if (requestCode == TAKE_PHOTO_FROM_CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            val imageBitmap = data.extras?.get("data") as Bitmap
            imageView.setImageBitmap(imageBitmap)
        }
    }

    private fun String.showInToast() =
        Toast.makeText(this@MainActivity, this, Toast.LENGTH_LONG).show()

    companion object {
        private const val LOG_TAG = "Camera app"

        private val ACCESS_DENIED_MESSAGE_ID = R.string.toast_message_access_denied
        private val ACCESS_GRANTED_MESSAGE_ID = R.string.toast_message_access_granted

        private const val CAMERA_PERMISSION = Manifest.permission.CAMERA
        private const val TAKE_PHOTO_FROM_CAMERA_REQUEST_CODE = 148
        private val IMAGE_ON_PERMISSION_REQUEST_DENY = R.drawable.marisad_cant_access_camera
    }
}